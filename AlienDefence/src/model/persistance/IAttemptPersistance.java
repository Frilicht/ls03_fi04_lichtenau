package model.persistance;

import java.util.Vector;

public interface IAttemptPersistance {

	int createHighscoreEntry(int F_user_id, int F_level_id, int shots, int hits, long reaction_time);
	int getPlayerPosition();
	Vector<Vector<String>> getAllAttemptsPerLevel(int level_id, int game_id);
	//no update needed
	void deleteHighscore(int level_id);
}